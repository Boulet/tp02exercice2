using System;
using Xunit;

using Exercice2Console;

namespace Exercice2Test
{
    public class OperationTest
    {
        [Fact]
        public void TestMultiplication()
        {
            Assert.Equal(4, Program.Multiplication(2, 2));
        }
    }
}